import math
import re
from operator import itemgetter
from nltk.corpus import stopwords as sw
from Utils import NLTKPreprocessor as pp
from pathlib import Path
import sys


class TfIdf:


  def __init__(self, corpus_filename = None,
               DEFAULT_IDF = 1.5):

    self.num_docs = 0
    self.term_num_docs = {}
    self.stopwords = set(sw.words('english'))
    self.idf_default = DEFAULT_IDF
    self.preprocessor = pp.NLTKPreprocessor('regular-token-pos')

    if corpus_filename is None:
        corpus_filename = ""
    idfFile = Path(corpus_filename)
    if corpus_filename and idfFile.is_file():
      corpus_file = open(corpus_filename, "r")

      # Load number of documents.
      line = corpus_file.readline()
      self.num_docs = int(line.strip())

      # Reads "term:frequency" from each subsequent line in the file.
      for line in corpus_file:
       tokens = line.split(":")
       term = tokens[0].strip()
       frequency = int(tokens[1].strip())
       self.term_num_docs[term] = frequency


  def get_tokens(self, str):
    tokens = self.preprocessor.tokenize(str)
    return tokens

  def combine_files(self, inputFiles, outputFile):
      totalDocs = 0
      for file in inputFiles:
        idfFile = Path(file)
        if file and idfFile.is_file():
            corpus_file = open(file, "r")

          # Load number of documents.
            line = corpus_file.readline()
            totalDocs += int(line.strip())

          # Reads "term:frequency" from each subsequent line in the file.
            for line in corpus_file:
                tokens = line.split(":")
                term = tokens[0].strip()
                frequency = int(tokens[1].strip())
                if(term in self.term_num_docs):
                    self.term_num_docs[term] += frequency
                else:
                    self.term_num_docs[term] = frequency
      self.num_docs = totalDocs
      self.save_corpus_to_file(outputFile)

  def add_input_document(self, input):
    """Add terms in the specified document to the idf dictionary."""
    self.num_docs += 1
    tokens = self.get_tokens(input)
    words = set(tokens)

    for word in words:
      if word[0] in self.term_num_docs:
        self.term_num_docs[word[0]] += 1
      else:
        self.term_num_docs[word[0]] = 1

  def save_corpus_to_file(self, idf_filename):
    output_file = open(idf_filename, "w")

    output_file.write(str(self.num_docs) + "\n")
    for term, num_docs in self.term_num_docs.items():
      output_file.write(str(term) + ": " + str(num_docs) + "\n")

  def get_num_docs(self):
    """Return the total number of documents in the IDF corpus."""
    return self.num_docs

  def get_idf(self, term):
    if term in self.stopwords:
      return 0

    if not term in self.term_num_docs:
      return self.idf_default

    return math.log(float(1 + self.get_num_docs()) / 
      (1 + self.term_num_docs[term]))

  def get_doc_keywords(self, curr_doc):
    tfidf = {}
    tokens = list(self.get_tokens(curr_doc))

    seen = set()
    unique = []
    for obj in tokens:
        if obj[0] not in seen:
            unique.append(obj)
            seen.add(obj[0])

    for word in unique:
        wordCount = sum(p[0] == word[0] for p in tokens)
        mytf = float(wordCount) / len(unique)
        myidf = self.get_idf(word[0])
        tfidf[word] = mytf * myidf

    return sorted(tfidf.items(), key=itemgetter(1), reverse=True)