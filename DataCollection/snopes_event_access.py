import csv
from pathlib import Path
#from WebSiteCrawl import *
import threading


def process_events_urls_to_claims():

    numberOfThreads = 8
    wrong_urls = ''
    correct_claims = ''
    threads = []
    with open('../Data/snopes-events.txt', 'r') as csvfile:
        reader = list(csv.reader(csvfile, delimiter = '	'))

        snopesChunks = chunks(reader,numberOfThreads)
        try:
            for c in snopesChunks:
                threads.append(threading.Thread(target=processMultipleUrls,args=(c, wrong_urls, correct_claims)))
            for x in threads:
                x.start()
            for x in threads:
                x.join()
        except Exception:
            print("Exception")


    with open('../Data/testwrong_urls' + '.txt', 'w+') as outfileWrong:
        outfileWrong.write(wrong_urls)

    with open('../Data/testclaims' + '.csv', 'w+') as outfileClaims:
        outfileClaims.write(correct_claims)


def processMultipleUrls(urls, wrong_urls, correct_claims):
    pass
    #for url in urls:
    #    text = get_claims_heading(url[2])
    #    if text != '' and text is not None:
    #        correct_claims += url[0] + '	'+ url[1] + '	' + text + '\r\n'
    #    else:
    #       wrong_urls += url[2] + '\r\n'



#TODO UnitTest this
def chunks(l, n):
    length = len(l)
    chunkSize = length/n
    for i in range(0, length, chunkSize):
        yield l[i:i + n]

