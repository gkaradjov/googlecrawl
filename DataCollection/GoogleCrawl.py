from googleapiclient.discovery import build


class GoogleSearch(object):
    my_api_key = "AIzaSyD1cB05v4h_gHpFzWj6ynO8aVHNO4xKyzc"
    my_cse_id = "013999091253411575762:yudzfprsz5q"

    service = build("customsearch", "v1",
                developerKey=my_api_key)
    collection = service.cse()

    numberOfPages = 1

    def search(self, searchTerm, filename):
        result = []
        try:
            for i in range(0, self.numberOfPages):
                start_val = 1 + (i * 10)
                request = self.collection.list(q = searchTerm,
                                          start = start_val,
                                          cx=self.my_cse_id
                                          )
                result.append(request.execute())

            # Select every item from each page and create list with search results only
            resultLinks = [ e['link'] for l in list(page['items'] for page in result) for e in l]
            resultSnippets = [ e['snippet'] for l in list(page['items'] for page in result) for e in l]
            resultDisplayURL = [ e['displayLink'] for l in list(page['items'] for page in result) for e in l]

            obj = {'links': resultLinks, 'snippets':resultSnippets, 'displayUrl' : resultDisplayURL, 'query': searchTerm}

            return obj
        except Exception as e:
            print("query:" + searchTerm + "; filename: " + str(filename)+ "; exception: " + str(e))
            return {'links': [], 'snippets':[], 'displayUrl' : []}
    