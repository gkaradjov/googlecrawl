
import http.client, urllib.request, urllib.parse, urllib.error
from Utils.data_access import *

headers = {
    # Request headers
    'Ocp-Apim-Subscription-Key': '567f50f112e549f4a0639b097c403bc3',
}
#searchTerm = "\"qtel\" \"#\" customer service number find"

def bingSearch(searchTerm, excludeDomains, filename):
    params = urllib.parse.urlencode({
        # Request parameters
        'q': searchTerm,
        'count': '20',
        'offset': '0',
        'mkt': 'en-us',
        'safesearch': 'Moderate',
    })

    try:
        conn = http.client.HTTPSConnection('api.cognitive.microsoft.com')
        conn.request("GET", "/bing/v5.0/search?%s" % params, "{body}", headers)
        response = conn.getresponse()
        print(response)
        data = response.read()
        d = json.loads(data.decode(encoding='utf-8'))
        conn.close()
        return d


    except Exception as e:
        print(str(e))


def postProcessBingResults( file_id, excludeDomain, maxResults, writeFile):
    links = []
    try:
        with open( '../Data/BingWebsiteLinks/Factchecking/' + str(file_id) + '.json', 'r+') as data_file:
            jstr = data_file.read()
            links = json.loads(jstr)
            count = 0
            data = {}
            data['links'] = []
            data['snippets'] = []
            data['displayUrl'] = []
            webPages = links['webPages']['value']
            for wp in webPages:
                if(count == 10):
                    break
                if(excludeDomain in wp['displayUrl']):
                    continue
                data['snippets'].append(wp['snippet'])
                data['links'].append(wp['url'])
                data['displayUrl'].append(wp['displayUrl'])
                count += 1
            resultString = json.dumps(data, sort_keys=True, indent=2)

            with open('../Data/BingWebsiteLinks/FactChecking/Postprocessed/' + writeFile +'.json', 'w') as outfile:
                outfile.write(resultString)
    except:
        print("no results")
