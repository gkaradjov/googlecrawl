from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn import metrics
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
import csv
from Utils.data_access import *

from Utils.NLTKPreprocessor import *

def addFeatureFile(currentFeatures, newFilePath):

    with open(newFilePath, 'r+') as readFile:
        reader = csv.reader(readFile, delimiter='\t', lineterminator='\n')
        for record in reader:
            key = record[0]
            if key in currentFeatures:
                currentFeatures[key].extend(record[1:])
            else:
                currentFeatures[key] = record[1:]


def run(runName, featureFiles):
    import datetime
    print(str(datetime.datetime.now()) + " ] starting run:" + runName)
    from collections import defaultdict
    features = defaultdict(dict)
    pathToFeatureFiles = '../Data/FeatureSets/'

    for f in featureFiles:
        newFilePath = pathToFeatureFiles + str(f)
        addFeatureFile(features,newFilePath)

    claims = get_claims()
    X = []
    Y = []
    for claim in claims:
        X += [features[claim[0]]]
        Y.append(claim[1])

    from sklearn.model_selection import KFold
    from sklearn.model_selection import GridSearchCV
    from sklearn.svm import SVC

    param_grid = [
        {'C': [1, 2, 4, 8, 16, 32, 64, 100, 128], 'kernel': ['linear'], 'probability': [True]},
        {'C': [1, 2, 4, 8, 16, 32, 64, 100, 128], 'gamma': [2, 1, 0.5, 0.3, 0.2, 0.1, 0.01, 0.001], 'kernel': ['rbf'], 'probability': [True]},
    ]
    Xtrain = []
    Ytrain = []
    kFold = KFold(n_splits=10, shuffle=True, random_state=1)
    rumorActual = 0
    notRumorActual = 0
    print("Gridsearch start")
    clfTunned = GridSearchCV(SVC(C=1), param_grid, cv=kFold,
                       scoring='accuracy', n_jobs=6)
    clfTunned.fit(X, Y)
    print(clfTunned.best_score_)

    clf = clfTunned.best_estimator_

    truePositive = 0
    trueNegative = 0
    falseNegative = 0
    falsePositive = 0

    print("Gridsearch end, Eval start")

    for train, test in kFold.split(X):
        Xtrain = np.asarray([X[t] for t in train], float)
        Ytrain = [Y[t] for t in train]
        Xtest = np.asarray([X[t] for t in test], float)
        Ytest = [Y[t] for t in test]


        clf = clf.fit(Xtrain, Ytrain)
        predicted = clf.predict(Xtest)

        for p, g in zip(predicted, Ytest):
            if g == 'rumor':
                rumorActual += 1
                if g == p:
                    truePositive += 1
                else:
                    falseNegative += 1
            else:
                notRumorActual += 1
                if g == p:
                    trueNegative += 1
                else:
                    falsePositive += 1


    accuracy = (truePositive + trueNegative) / (falseNegative + truePositive + falsePositive + trueNegative)
    precission = (truePositive)/(truePositive + falsePositive)
    recall = (truePositive) / (truePositive + falseNegative)
    fScore = 2*(precission*recall)/(precission + recall)

    with open('run-results.tsv','a', newline='') as csvFile:
        writer = csv.writer(csvFile, delimiter='\t')
        writer.writerow([runName,str(datetime.datetime.now().date()),truePositive,trueNegative,falsePositive,falseNegative,accuracy,precission,recall,fScore,clfTunned.best_params_,rumorActual,notRumorActual])

#"IR-Bing-vectors",
#"IR-Google-vectors

if __name__ == '__main__':
    from multiprocessing import freeze_support
    freeze_support()

    #50 dim
   # run('bingWordVectors_50d', ['IR-Bing-vectors_50d'])
   # run('googleWordVectors_50d', ['IR-Google-vectors_50d'])
   # run('allVectors_50d', ['IR-Bing-vectors_50d','IR-Google-vectors_50d'])
   # run('bingSnippets_wordvec_50d', ['IR-Similarity-bing-snippets', 'IR-Bing-vectors_50d'])
   # run('bingWebpages_wordvec_50d', ['IR-Similarity-bing-webpages', 'IR-Bing-vectors_50d'])
   #run('bingAll_wordvec_50d', ['IR-Similarity-bing-snippets', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_50d'])
   #run('googleSnippets_wordvec_50d', ['IR-Similarity-google-snippets', 'IR-Google-vectors_50d'])
   #run('googleWebpages_wordvec_50d', ['IR-Similarity-google-webpages', 'IR-Google-vectors_50d'])
   #run('googleAll_wordvec_50d', ['IR-Similarity-google-snippets', 'IR-Similarity-google-webpages', 'IR-Google-vectors_50d'])
   #run('snippetsAll_wordvec_50d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets', 'IR-Bing-vectors_50d', 'IR-Google-vectors_50d'])
   #run('webPagesAll_wordvec_50d', ['IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_50d', 'IR-Google-vectors_50d'])
   #run('IR-All_wordvec_50d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets',
   #                           'IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_50d', 'IR-Google-vectors_50d'])


    #run('bingWordVectors_100d', ['IR-Bing-vectors_100d'])
    #run('googleWordVectors_100d', ['IR-Google-vectors_100d'])
    #run('allVectors_100d', ['IR-Bing-vectors_100d','IR-Google-vectors_100d'])
    #run('bingSnippets_wordvec_100d', ['IR-Similarity-bing-snippets', 'IR-Bing-vectors_100d'])
    #run('bingWebpages_wordvec_100d', ['IR-Similarity-bing-webpages', 'IR-Bing-vectors_100d'])
    #run('bingAll_wordvec_100d', ['IR-Similarity-bing-snippets', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_100d'])
    #run('googleSnippets_wordvec_100d', ['IR-Similarity-google-snippets', 'IR-Google-vectors_100d'])
    #run('googleWebpages_wordvec_100d', ['IR-Similarity-google-webpages', 'IR-Google-vectors_100d'])
    #run('googleAll_wordvec_100d', ['IR-Similarity-google-snippets', 'IR-Similarity-google-webpages', 'IR-Google-vectors_100d'])
    #run('snippetsAll_wordvec_100d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets', 'IR-Bing-vectors_100d', 'IR-Google-vectors_100d'])
    #run('webPagesAll_wordvec_100d', ['IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_100d', 'IR-Google-vectors_100d'])
    #run('IR-All_wordvec_100d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets',
    #                           'IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_100d', 'IR-Google-vectors_100d'])

    #run('bingWordVectors_200d', ['IR-Bing-vectors_200d'])
    #run('googleWordVectors_200d', ['IR-Google-vectors_200d'])
    #run('allVectors_200d', ['IR-Bing-vectors_200d','IR-Google-vectors_200d'])
    #run('bingSnippets_wordvec_200d', ['IR-Similarity-bing-snippets', 'IR-Bing-vectors_200d'])
    #run('bingWebpages_wordvec_200d', ['IR-Similarity-bing-webpages', 'IR-Bing-vectors_200d'])
    #run('bingAll_wordvec_200d', ['IR-Similarity-bing-snippets', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_200d'])
    #run('googleSnippets_wordvec_200d', ['IR-Similarity-google-snippets', 'IR-Google-vectors_200d'])
    #run('googleWebpages_wordvec_200d', ['IR-Similarity-google-webpages', 'IR-Google-vectors_200d'])
    #run('googleAll_wordvec_200d', ['IR-Similarity-google-snippets', 'IR-Similarity-google-webpages', 'IR-Google-vectors_200d'])
    #run('snippetsAll_wordvec_200d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets', 'IR-Bing-vectors_200d', 'IR-Google-vectors_200d'])
    #run('webPagesAll_wordvec_200d', ['IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_200d', 'IR-Google-vectors_200d'])
    #run('IR-All_wordvec_200d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets',
    #                           'IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_200d', 'IR-Google-vectors_200d'])
#
    #run('bingWordVectors_300d', ['IR-Bing-vectors_300d'])
    #run('googleWordVectors_300d', ['IR-Google-vectors_300d'])
  #run('allVectors_300d', ['IR-Bing-vectors_300d','IR-Google-vectors_300d'])
  #run('bingSnippets_wordvec_300d', ['IR-Similarity-bing-snippets', 'IR-Bing-vectors_300d'])
  #run('bingWebpages_wordvec_300d', ['IR-Similarity-bing-webpages', 'IR-Bing-vectors_300d'])
  #run('bingAll_wordvec_300d', ['IR-Similarity-bing-snippets', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_300d'])
  #run('googleSnippets_wordvec_300d', ['IR-Similarity-google-snippets', 'IR-Google-vectors_300d'])
  #run('googleWebpages_wordvec_300d', ['IR-Similarity-google-webpages', 'IR-Google-vectors_300d'])
  #run('googleAll_wordvec_300d', ['IR-Similarity-google-snippets', 'IR-Similarity-google-webpages', 'IR-Google-vectors_300d'])
  #run('snippetsAll_wordvec_300d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets', 'IR-Bing-vectors_300d', 'IR-Google-vectors_300d'])
  #run('webPagesAll_wordvec_300d', ['IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_300d', 'IR-Google-vectors_300d'])
    run('IR-All_wordvec_300d', ['IR-Similarity-bing-snippets', 'IR-Similarity-google-snippets',
                               'IR-Similarity-google-webpages', 'IR-Similarity-bing-webpages', 'IR-Bing-vectors_300d', 'IR-Google-vectors_300d'])