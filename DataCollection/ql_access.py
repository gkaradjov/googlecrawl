import csv
from pathlib import Path

def readQLFile():
    claims = []
    my_file = Path('../Data/QLTestSet.tsv')
    counter = 0
    if my_file.is_file():
        with open('../Data/QLTestSet.tsv', 'r+') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                if counter == 0:
                    counter += 1
                    continue
                claims.append([row[1], row[5], row[2], row[3]])
    else:
        print("file doesn't exist")
    return claims

