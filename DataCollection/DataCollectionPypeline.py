from DataCollection.GoogleCrawl import GoogleSearch
from DataCollection.BingCrawl import *
import Utils.KeyWordExtraction as kwe
import Utils.TfIdf as tf
import copy

def PrepareQueryAndSearch(dataList, searchFunction, elementsSelector, filepath):
    count = 0
    tfidif = tf.TfIdf("QLIDF.txt")
    for item in dataList:
        pureText = item[2]
        heading = kwe.getKeyPhrases(tfidif.get_doc_keywords(pureText), pureText, "QA" + str(count))
        resultCount = 0
        tries = 0
        while resultCount < 9:
            tries +=1
            search_text = " "
            for gr in heading:
                search_text += " ".join(gr)
                search_text += " "

            results = searchFunction(search_text, item[1])
            resultCopy = copy.deepcopy(results)

            for element in elementsSelector:
                if resultCopy and element in resultCopy:
                    resultCopy = resultCopy.get(element)
                else:
                    resultCount = -1
                    break
            if resultCount != -1:
                resultCount = len(resultCopy)
            
            if len(heading[0]) > len(heading[1]):
                heading[0] = heading[0][:-1]
                continue
            elif len(heading[1]) > len(heading[2]):
                heading[1] = heading[1][:-1]
            else:
                heading[2] = heading[2][:-1]

            if tries == 15:
                break

        with open( filepath + str(item[1]) + '.json', 'w') as outfile:
            json.dump(results, outfile, indent=2)
        count += 1

def ProcessGoogle(dataList):
    googleCSE = GoogleSearch()
    PrepareQueryAndSearch(dataList, googleCSE.search, ["links"], '../Data/GoogleWebsiteLinks/QL/QLOnly/')

def ProcessBing(dataList):
    PrepareQueryAndSearch(dataList, bingSearch, ["webPages", "value"])
    for data in dataList:
        postProcessBingResults(data[0], '', 10, data[0])

def Run(items):
    data = readQLFile()

    if "ProcessGoogle" in items:
        ProcessGoogle(data)
    if "ProcessBing" in items:
        ProcessBing(data)
    pass



if __name__ == '__main__':
    Run(["ProcessGoogle"])