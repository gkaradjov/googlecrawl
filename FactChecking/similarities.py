sentense1 = "Well importaant done intruded !!!"
sentense2 = "you, are nicely importaant intruded importaant done intruded!"

from sklearn.feature_extraction.text import TfidfVectorizer
from Utils.NLTKPreprocessor import *
import numpy as np
import csv
from pathlib import Path
from scipy import spatial
from nltk.util import ngrams
from nltk.tokenize import sent_tokenize
from Utils.data_access import *
import copy
import statistics
import json
import sys
wordEmbedings = None


def readEmbedings(vector_size):
    embedings = dict()
    #my_file = Path('../Data/glove.6B.'+str(vector_size)+'d.txt')
    my_file = Path('../Data/embedings.tsv')

    if my_file.is_file():
        with open('../Data/embedings.tsv', 'r+', encoding="utf8" ) as csvfile:
        #with open('../Data/glove.6B.'+str(vector_size)+'d.txt', 'r+', encoding="utf8" ) as csvfile:
            reader = csv.reader(csvfile, delimiter=' ', quoting=csv.QUOTE_NONE)
            for row in reader:
                embedings[row[0]] = np.array(row[1:]).astype(np.float)
    else:
        print("file doesn't exist")
    global wordEmbedings
    wordEmbedings = embedings


prerpocessorNLTK = NLTKPreprocessor('regular').preprocessString
tokenizerRaw = NLTKPreprocessor('regular').tokenizeDocumentRaw
tokenizerLemma = NLTKPreprocessor('regular').tokenizeDocument
vectorizer = TfidfVectorizer(preprocessor=prerpocessorNLTK)


def calculateTfIdfSimilarity(text1, text2):
    tfidf = vectorizer.fit_transform([text1, text2])
    return ((tfidf * tfidf.T).A)[0, 1]


def getTextVector(text, embedingSize):
    if not wordEmbedings:
        readEmbedings(embedingSize)

    text1WordEmbedings = []
    text1Tokens = tokenizerRaw(text)
    for token in text1Tokens:
        if token in wordEmbedings:
            text1WordEmbedings.append(wordEmbedings[token])
    text1Vector = np.sum(text1WordEmbedings, axis=0) / len(text1WordEmbedings)
    return text1Vector


def calculateWordEmbidingSimilarity(text1, text2, embedingSize):
    if not wordEmbedings:
        readEmbedings(embedingSize)

    text1Vector = getTextVector(text1, embedingSize)
    text2Vector = getTextVector(text2, embedingSize)


    if type(text1Vector) is not np.ndarray or type(text2Vector) is not np.ndarray:
        return 0
    result = 1 - spatial.distance.cosine(text1Vector, text2Vector)
    return result


def calculateContainmentMetric(text1, text2):
    if len(text1) > 3000:
        text1 = text1[:3000]
    tokens1 = tokenizerLemma(text1)
    tokens2 = tokenizerLemma(text2)
    text1trigrams = set(ngrams(tokens1, 3))
    text2trigrams = set(ngrams(tokens2, 3))

    coloca = len([x for x in text1trigrams if x in text2trigrams])

    if len(text2trigrams) == 0:
        return 0

    similarity = coloca / len(text2trigrams)

    return similarity


def fillOneArrayWithZeros(a):
    a = a + [0] * (10 - len(a))


def SetEverythingToZeros(data):
    fillOneArrayWithZeros(data['snippetToClaimTfIdf'])
    fillOneArrayWithZeros(data['snippetToClaimContainment'])
    fillOneArrayWithZeros(data['webPageToClaimContainment'])
    fillOneArrayWithZeros(data['webPageToClaimTfIdfAvg'])
    fillOneArrayWithZeros(data['webPageToClaimTfIdfMax'])

def get_most_simillar_sentences(similarity_object, websiteObject , index, embedingSize):
    if websiteObject:
        website = websiteObject["processedHtml"][index]
    questionComment = similarity_object["question"] + " " + similarity_object["answer"]
    #questionComment = similarity_object["claim"]

    from nltk import sent_tokenize
    website = website[:15000]
    sentences = sent_tokenize(website, language='english')
    grams = [sentences[i:i + 3] for i in range(len(sentences) - 3 + 1)]
    print("grams getted")
    simTfIdfClaim = []
    countGrams = 0
    bestSimilarity = 0
    bestGram = ""
    #if similarity_object["webPageToClaimWordVectorMax"][index] > 0:
    if similarity_object["webPageToquestionAnswerSimilarityWordVectorsMax"][index] > 0:
        for gram in grams:
            countGrams += 1
            gramText = ' '.join(gram)
            similarity = calculateWordEmbidingSimilarity(gramText, questionComment, embedingSize)
            if similarity > bestSimilarity:
                bestSimilarity = similarity
                bestGram = gramText
    return bestGram



def getMean(a):
    l = len(a)

    if l > 0:
        return np.mean(a).astype(float)
    else:
        return 0


def getMax(a):
    l = len(a)
    if l > 0:
        return np.max(a).astype(float)
    else:
        return 0


def getMin(a):
    l = len(a)

    if l > 0:
        return np.min(a).astype(float)
    else:
        return 0


def InitVariables(data):
    data['snippetToClaimTfIdf'] = []
    data['snippetToClaimContainment'] = []
    data['webPageToClaimContainment'] = []
    data['webPageToClaimTfIdfAvg'] = []
    data['webPageToClaimTfIdfMax'] = []
    data['webPageToClaimWordVectorAvg'] = []
    data['webPageToClaimWordVectorMax'] = []
    data['snippetToClaimWordVector'] = []


def calculateAllSimilarities(question, action, override):
    data = {'comment_id': question[0], 'claim': question[2], 'class':question[1]}

    InitVariables(data)

    inputFilePath = '../Data/CrawledWebsites/Factchecking/Google/' + str(question[0]) + '.json'
    outputFilePath = '../Data/Features/Factchecking/Google/' + str(question[0]) + '.json'
    my_file = Path(inputFilePath)
    outfilePath = Path(outputFilePath)
    if not outfilePath.is_file():
        if my_file.is_file():
            try:
                with open(inputFilePath,
                          'r+') as data_file:
                    links = json.load(data_file)

                data['links'] = list(links['links'])
                data['snippets'] = list(links['snippets'])
                data['displayUrl'] = list(links['displayUrl'])
                allWebsites = list(links['processedHtml'])
                print("Opening" + str(question[0]))
                for text in allWebsites:
                    print("calculate containment metric")
                    # for containment metric we can take the whole page
                    data['webPageToClaimContainment'].append(
                        calculateContainmentMetric(data['claim'], text))

                    text = text[:5000]
                    sentences = sent_tokenize(text, language='english')
                    grams = [sentences[i:i + 3] for i in range(len(sentences) - 3 + 1)]
                    print("grams getted")
                    simTfIdfClaim = []
                    countGrams = 0
                    for gram in grams:
                        countGrams += 1
                        if countGrams > 20:
                            break
                        gramText = ' '.join(gram)
                        simTfIdfClaim.append(
                            calculateTfIdfSimilarity(data['claim'], gramText))

                    data['webPageToClaimTfIdfAvg'].append(getMean(simTfIdfClaim))
                    data['webPageToClaimTfIdfMax'].append(getMax(simTfIdfClaim))


                for snippet in data["snippets"]:
                    data['snippetToClaimTfIdf'].append(
                        calculateTfIdfSimilarity(data['claim'], snippet))
                    #data['snippetToquestionAnswerSimilarityWordVectors'].append(
                    #    calculateWordEmbidingSimilarity(data['question'] + " " + data['answer'], snippet))
                    data['snippetToClaimContainment'].append(
                        calculateContainmentMetric(data['claim'], snippet))

            except Exception as e:
                print(e)
                SetEverythingToZeros(data)
                print("error")
        else:
            data['links'] = ['file_not_found']
            data['snippets'] = ['file_not_found']
            data['displayUrl'] = ['file_not_found']

            SetEverythingToZeros(data)
            print("file not found")
        try:
            resultString = json.dumps(data, sort_keys=True, indent=2)
            with open(outputFilePath,
                      'w') as outfile:
                outfile.write(resultString)
        except Exception as e:
            print(e)

def updateSimilarity(question, updateCollection):
    data = {}
    inputFilePath = '../Data/CrawledWebsites/Factchecking/Google/' + str(question[0]) + '.json'
    outputFilePath = '../Data/Features/Factchecking/Google/' + str(question[0]) + '.json'
    my_file = Path(inputFilePath)
    outfilePathObj = Path(outputFilePath)
    vectorSize = 200

    if not my_file.is_file() or not outfilePathObj.is_file():
        return
    try:
        with open(inputFilePath,
                  'r+') as data_file:
            links = json.load(data_file)

        with open(outputFilePath,
                  'r+') as data_file:
            data = json.load(data_file)

        if "wordVectorSimilarities" in updateCollection:
            data['webPageToClaimWordVectorAvg'] = []
            data['webPageToClaimWordVectorMax'] = []
            data['snippetToClaimWordVector'] = []

            allWebsites = list(links['processedHtml'])
            print("Opening" + str(question[0]))
            for text in allWebsites:
                text = text[:5000]
                sentences = sent_tokenize(text, language='english')
                grams = [sentences[i:i + 3] for i in range(len(sentences) - 3 + 1)]
                print("grams getted")
                wordEmbedingSim = []
                countGrams = 0
                for gram in grams:
                    countGrams += 1
                    if countGrams > 20:
                        break
                    gramText = ' '.join(gram)
                    wordEmbedingSim.append(
                        calculateWordEmbidingSimilarity(data['claim'], gramText, 300))

                data['webPageToClaimWordVectorAvg'].append(getMean(wordEmbedingSim))
                data['webPageToClaimWordVectorMax'].append(getMax(wordEmbedingSim))

                for snippet in data["snippets"]:
                    data['snippetToClaimWordVector'].append(
                        calculateWordEmbidingSimilarity(data['claim'], snippet, 300))
        if "wordVectorSummary" in updateCollection:
            data["webPagesWordVectors" + str(vectorSize)] = []

            for index, item in enumerate(links["processedHtml"]):
                mostSimmilar = get_most_simillar_sentences(data, links, index, vectorSize)
                websiteVector = getTextVector(mostSimmilar, vectorSize)
                data["webPagesWordVectors" + str(vectorSize)].append(websiteVector.tolist())

            data["claimWordVector" + str(vectorSize)] = []
            claimWordVec = getTextVector(question[2], vectorSize)
            data["claimWordVector" + str(vectorSize)] = claimWordVec.tolist()

            data["snippetsWordVector" + str(vectorSize)] = []
            for snippet in data["snippets"]:
                snippetVector = getTextVector(snippet, vectorSize)
                data["snippetsWordVector" + str(vectorSize)].append(snippetVector.tolist())


            data["tweetWordVector" + str(vectorSize)] = []
            tweets = get_tweets_for_claim(question[0])
            tweetText = " ".join(tweets)
            tweetWordVec = getTextVector(tweetText, vectorSize)
            data["tweetWordVector" + str(vectorSize)] = tweetWordVec.tolist()

    except Exception as e:
        print(e)
        SetEverythingToZeros(data)
        print("error")
    try:
        resultString = json.dumps(data, sort_keys=True, indent=2)
        with open(outputFilePath,
                  'w') as outfile:
            outfile.write(resultString)
    except Exception as e:
        print(e)


def get_snippet_metrics(data, row, key):
    workingArray = data[key]
    if (len(workingArray) == 0):
        row.extend([0.0, 0.0, 0.0, 0.0])
    else:
        row.append(workingArray[0])  # score at the first result
        row.append(getMax(workingArray[:3]))
        row.append(getMean(workingArray))
        row.append(getMax(workingArray))



def get_link_vectors(links, link_stats):
    result = []
    tempFound = False
    for link in links:
        tempFound = False
        for link_stat in link_stats:
            if link_stat[0] in statistics.getDomain(link):
                result.append([int(link_stat[2]),int(link_stat[3]),int(link_stat[4]),int(link_stat[5])])
                tempFound = True
                break
        if tempFound == False:
            result.append([0,0,0,1])
    return result

def filterArray(datasource, key, filtervector):
    datasource[key] = [x for x in datasource[key] if datasource[key].index(x) in filtervector]


def get_specific_texts(data_source,link_stats, qatar_related=False, not_qatar_related=False,
                       reputed=False, forum=False, others=False):

    new_data = copy.deepcopy(data_source)

    counter = 0
    indexes = []
    print(new_data["comment_id"])
    displayUrls = []
    if "displayUrl" in new_data:
        displayUrls = new_data["displayUrl"]
    else:
        displayUrls = new_data["links"]
    vec = get_link_vectors(displayUrls, link_stats)

    qatar_related_vec = [vec.index(x) for x in vec if (x[0] == 1 and qatar_related == True) or (x[0]==0 and not_qatar_related == True)]
    source_vec = [vec.index(x) for x in vec if (vec.index(x) in qatar_related_vec)
                                and ((x[1] == 1 and reputed == True)
                                or (x[2]==1 and forum == True)
                                or (x[3]==1 and others == True))]

    filterArray(new_data, "snippetToquestionAnswerSimilarityTfIdf", source_vec)
    #filterArray(new_data, "snippetToquestionAnswerSimilarityWordVectors", source_vec)
    filterArray(new_data, "snippetToquestionAnswerSimilarityContainment", source_vec)
    filterArray(new_data, "snippetToQuestionSimilarityTfIdf", source_vec)
    #filterArray(new_data, "snippetToQuestionSimilarityWordVectors", source_vec)
    filterArray(new_data, "snippetToQuestionSimilarityContainment", source_vec)
    filterArray(new_data, "snippetToAnswerSimilarityTfIdf", source_vec)
    #filterArray(new_data, "snippetToAnswerSimilarityWordVectors", source_vec)
    filterArray(new_data, "snippetToAnswerSimilarityContainment", source_vec)
    filterArray(new_data, "webPageToquestionAnswerSimilarityContainment", source_vec)
    filterArray(new_data, "webPageToQuestionSimilarityContainment", source_vec)
    filterArray(new_data, "webPageToAnswerSimilarityContainment", source_vec)
    filterArray(new_data, "webPageToquestionAnswerSimilarityTfIdfAvg", source_vec)
    filterArray(new_data, "webPageToquestionAnswerSimilarityTfIdfMax", source_vec)
    #filterArray(new_data, "webPageToquestionAnswerSimilarityWordVectorsAvg", source_vec)
    #filterArray(new_data, "webPageToquestionAnswerSimilarityWordVectorsMax", source_vec)
    filterArray(new_data, "webPageToQuestionSimilarityTfIdfAvg", source_vec)
    filterArray(new_data, "webPageToQuestionSimilarityTfIdfMax", source_vec)
    #filterArray(new_data, "webPageToQuestionSimilarityWordVectorsAvg", source_vec)
    #filterArray(new_data, "webPageToQuestionSimilarityWordVectorsMax", source_vec)
    filterArray(new_data, "webPageToAnswerSimilarityTfIdfAvg", source_vec)
    filterArray(new_data, "webPageToAnswerSimilarityTfIdfMax", source_vec)
  #  filterArray(new_data, "webPageToAnswerSimilarityWordVectorsAvg", source_vec)
   # filterArray(new_data, "webPageToAnswerSimilarityWordVectorsMax", source_vec)

    return new_data


def extract_all_webpage_features(dataSource, row):
    get_snippet_metrics(dataSource, row, 'webPageToClaimContainment')
    get_snippet_metrics(dataSource, row, 'webPageToClaimTfIdfAvg')
    get_snippet_metrics(dataSource, row, 'webPageToClaimTfIdfMax')
    get_snippet_metrics(dataSource, row, 'webPageToClaimWordVectorAvg')
    get_snippet_metrics(dataSource, row, 'webPageToClaimWordVectorMax')




def extract_all_snippets(dataSource, row):
    get_snippet_metrics(dataSource, row, 'snippetToClaimTfIdf')
    get_snippet_metrics(dataSource, row, 'snippetToClaimContainment')
    get_snippet_metrics(dataSource, row, 'snippetToClaimWordVector')

def extract_word_vectors(dataSource, row):
    vectorSize = 300
    webVectors = [np.array(a) for a in dataSource["webPagesWordVectors" + str(vectorSize)]]
    webVectors = [i for i in webVectors if np.shape(i) == (vectorSize,)]
    if len(webVectors) == 0:
        row.extend([0] * vectorSize)
    else:
        comulatative  = np.sum(webVectors, axis=0) / len(webVectors)
        row.extend(comulatative.tolist())


    snippetVectors = [np.array(a) for a in dataSource["snippetsWordVector"+ str(vectorSize)]]
    snippetVectors = [i for i in snippetVectors if np.shape(i) == (vectorSize,)]
    if len(snippetVectors) == 0:
        row.extend([0] * vectorSize)
    else:
        comulatative = np.sum(snippetVectors, axis=0) / len(snippetVectors)
        row.extend(comulatative.tolist())


    rawVec = dataSource["tweetWordVector"+ str(vectorSize)]
    twitterVector = np.array(rawVec)
    if np.shape(twitterVector) == (vectorSize,):
        row.extend(twitterVector.tolist())
    else:
        row.extend([0] * vectorSize)

    rawVec = dataSource["claimWordVector" + str(vectorSize)]
    twitterVector = np.array(rawVec)
    if np.shape(twitterVector) == (vectorSize,):
        row.extend(twitterVector.tolist())
    else:
        row.extend([0] * vectorSize)




def add_site_statistics_feature(bing, row):
    pass


def createSimilarityFeatureFile(outputFileName, variant, withSnippets=False, withHtml=False, forBing=False, forGoogle=False, withVectors=False ):
    questions = get_claims()
    counter = 0
    result = []
    link_stats = []
    with open('../Data/siteOcurancesprocessed.tsv', 'r') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
           link_stats.append(row)
    for question in questions:
        print(outputFileName + question[0])
        row = []
        my_file = Path('../Data/Features/Factchecking/Bing/' + str(question[0]) + '.json')
        if my_file.is_file():
            with open('../Data/Features/Factchecking/Bing/' + str(question[0]) + '.json', 'r+') as data_file:
                dataBing = json.load(data_file)
            with open('../Data/Features/Factchecking/Google/' + str(question[0]) + '.json', 'r+') as data_file:
                dataGoogle = json.load(data_file)
        row.append(dataBing['comment_id'])
        # snippetFeatures

        bingVariants = []
        googleVariants = []
        #qatarRelated = get_specific_texts(dataBing, link_stats, qatar_related=True, not_qatar_related=False )

        #no filtering of websites
        if variant == 0:
            bingVariants.append(dataBing)
            googleVariants.append(dataGoogle)

        #qatar only resutls
        if variant == 1:
            if forBing:
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, reputed=True,forum=True, others=True))

            if forGoogle:
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, reputed=True,forum=True, others=True))

        #qatar only resutls split into three sets
        if variant == 2:
            if forBing:
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, reputed=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, forum=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, others=True))

            if forGoogle:
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, reputed=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, forum=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, others=True))

        #qatar only resutls split into three sets
        if variant == 3:
            if forBing:
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, reputed=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, forum=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, others=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, not_qatar_related=True, reputed=True,forum=True, others=True))

            if forGoogle:
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, reputed=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, forum=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, others=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, not_qatar_related=True, reputed=True,forum=True, others=True))

        #qatar and not-qatar resutls split into three sets
        if variant == 4:
            if forBing:
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, reputed=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, forum=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, others=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, not_qatar_related=True, reputed=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, not_qatar_related=True, forum=True))
                bingVariants.append(get_specific_texts(dataBing, link_stats, not_qatar_related=True, others=True))
            if forGoogle:
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, reputed=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, forum=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, others=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, not_qatar_related=True, reputed=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, not_qatar_related=True, forum=True))
                googleVariants.append(get_specific_texts(dataGoogle, link_stats, not_qatar_related=True, others=True))

        #the regular feature just with simple augumentation with website stats
        if variant == 5:
            bingVariants.append(get_specific_texts(dataBing, link_stats, qatar_related=True, reputed=True,forum=True, others=True))
            googleVariants.append(get_specific_texts(dataGoogle, link_stats, qatar_related=True, reputed=True,forum=True, others=True))

        if forBing:
            for bing in bingVariants:
                if withSnippets:
                    extract_all_snippets(bing, row)
                if (withHtml):
                    extract_all_webpage_features(bing, row)
                if variant == 5:
                    add_site_statistics_feature(bing, link_stats, row)
                if withVectors:
                    extract_word_vectors(bing, row)
        if forGoogle:
            for google in googleVariants:
                if withSnippets:
                    extract_all_snippets(google, row)
                if (withHtml):
                    extract_all_webpage_features(google, row)
                if variant == 5:
                    add_site_statistics_feature(google, link_stats, row)
                if withVectors:
                    extract_word_vectors(google, row)

        result.append(row)

    with open('../Data/FeatureSets/' + outputFileName, 'w') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t', lineterminator='\n')
        for record in result:
            writer.writerow(record)


if __name__ == "__main__":
    createSimilarityFeatureFile("IR-Bing-vectors_300d", 0, forBing=True, withVectors=True )
    createSimilarityFeatureFile("IR-Google-vectors_300d", 0, forGoogle=True, withVectors=True )

    #createSimilarityFeatureFile("IR-Similarity-bing-snippets", 0, withSnippets=True, forBing=True)
    #createSimilarityFeatureFile("IR-Similarity-google-snippets", 0, withSnippets=True, forGoogle=True)
    #createSimilarityFeatureFile("IR-Similarity-bing-webpages", 0, withHtml=True, forBing=True)
    #createSimilarityFeatureFile("IR-Similarity-google-webpages", 0, withHtml=True, forGoogle=True)
    #questions = reversed(get_claims())
    #####
    #for question in questions:
    #    updateSimilarity(question, ["wordVectorSummary"])
