from Utils.KeyWordExtraction import *
from Utils.NERWatson import *
from pathlib import Path
from Utils.data_access import *
import csv

allowedValues = ["Factual - True", "Factual - False", "Factual - Partially True", "Factual - Conditionally True", "NonFactual", "Factual - Responder Unsure"]


def writeQLFile(data, filename):

    with open(filename,'w', newline='') as csvFile:
        writer = csv.writer(csvFile, delimiter = '\t')
        writer.writerow(["QID", "CID", "Question", "Comment", "Label", "Query_QuestionComment", "Query_Question", "Query_comment"])
        for item in data:
            writer.writerow(item)

data = get_claims()
query= []


tfidif = tf.TfIdf("snopes.txt")

#for claim in data:
#    tfidif.add_input_document(claim[2])

#tfidif.save_corpus_to_file("snopes.txt")
filtered = data
count = 0
for d in filtered:
    #if count >= 153:
    #combined question and comment

    claim = d[2]
    saveNamedEntitiesToFile(claim, "C" + str(d[0]))
    #commentKeywords = getKeyPhrases(tfidif.get_doc_keywords(claim), claim, "C" + str(d[0]))
    #d.append(commentKeywords)

    #questionOnly = d[2]
    ##saveNamedEntitiesToFile(questionOnly, "Q" + str(d[1]))
    #questionOnlyKeyWords = getKeyPhrases(tfidif.get_doc_keywords(questionOnly), questionOnly, "Q" + str(d[1]))
    #d.append(questionOnlyKeyWords)
#
#
    #commentOnly = d[3]
    ##saveNamedEntitiesToFile(commentOnly, "A" + str(d[1]))
    #commentOnlyKeyWords = getKeyPhrases(tfidif.get_doc_keywords(commentOnly), commentOnly, "A" + str(d[1]))
    #d.append(commentOnlyKeyWords)
    #count += 1

#writeQLFile(filtered, "QLTestSet.tsv")

