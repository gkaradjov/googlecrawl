__author__ = 'Gergi'
import time

from multiprocessing.dummy import Pool as ThreadPool

pool = ThreadPool(6)

def map(operation, collection):
    results = pool.map(operation,collection)
    pool.close()
    pool.join()
    return results


def waitAndPrint(collection):
    new = {}
    new['test'] = []
    new['test'].append(collection)
    new['test'].append(collection)
    time.sleep(collection)
    new['test'].append(collection)
    new['test'].append(collection*collection)
    print(new)




#collect = [1, 12, 3, 15, 15,2]


#map(waitAndPrint, collect)