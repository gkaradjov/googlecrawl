import os
import sys


def get_total_lines(path='', fileExtensions=None):
    total_lines = 0
    path = os.getcwd()
    if not fileExtensions:
        fileExtensions = [".py", ".cs", ".sql", ".js"]
    for directory, dirnames, files in os.walk(path):
        for f in files:
            filename, file_extension = os.path.splitext(f)
            if file_extension in fileExtensions:
                full_path = os.path.join(directory, f)
                with open(full_path, 'r') as fw:
                    lines_in_file = [x for x in fw.readlines() if x != "\n"]
                    total_lines += len(lines_in_file)
                    fw.close()


    print("Total Lines of Code in the Project - ", total_lines)

if __name__ == '__main__':
    get_total_lines()
    sys.exit()