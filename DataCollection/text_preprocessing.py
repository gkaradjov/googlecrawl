import re


def remove_leading_quotes(text):
    textLen = len(text)
    cleanText = text.strip()
    cleanText = re.sub("[\'\"-+@$#*]", '', cleanText)
    return cleanText

def get_first_n_words(text, numberOfWords, numberOfCharacters):
    textArray = text.split()
    joined = ' '.join(textArray[:numberOfWords])
    if len(textArray) < numberOfWords or len(joined) < numberOfCharacters:
        return text
    else:
        return joined
