from watson_developer_cloud import AlchemyLanguageV1
import json


myText = "Is there any Carrefour which is open ?? Please update. Thanks in advance. You can go to Landmark or Carrefour Market at abu humor"



def saveNamedEntitiesToFile(text, fileName):
    alchemy_language = AlchemyLanguageV1(api_key='1ace4464493743e3dce62a215d2ec96ae7aeb06c')
    with open('Entities/' + fileName + ".json", 'w') as outfile:
        json.dump(alchemy_language.entities(text,language='english'), outfile, indent=2)

def readNamedEntitiesFromFile(fileName):
    with open('../Utils/Entities/' + fileName + ".json") as data_file:
        data = json.load(data_file)

    relevantData = [{"text": tc["text"], "relevance": tc["relevance"], "type": tc["type"]} for tc in data["entities"]]
    relevantData.sort(key=lambda x: x["relevance"], reverse=True)

    return relevantData

