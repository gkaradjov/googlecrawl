from Utils import TfIdf as tf
import xml.etree.ElementTree as ET
from Utils.NERWatson import *


def get_data_vocabulary():
    tfidif = tf.TfIdf("QLIDF5.txt")
    print("Open document")
    root = ET.parse('../Data/QLCorpus.xml').getroot()
    count = 0
    print("Start processing")
    for sequence in root:
        count += 1
        if 130001 <= count <= 190000:
            if count % 100 == 0:
                print(count)
            innerTextArray = []
            for el in sequence:
                innerTextArray += [inn.text for inn in el if inn.text is not None]
            innerText = str.join(" ",innerTextArray)
            tfidif.add_input_document(innerText)

    print("Saving corpus to file")
    tfidif.save_corpus_to_file("QLIDF5.txt")
    print("Done!")
    #claims = []
    #my_file = Path('../Data/event_claim_clean.csv')
    #if my_file.is_file():
    #    with open('../Data/event_claim_clean.csv', 'r+') as csvfile:
    #        reader = csv.reader(csvfile, delimiter='\t')
    #        for rows in reader:
    #            claims.append(rows)
    #return claims

def get_data_test():
    return get_data_vocabulary()

def getKeyPhrases(listOfTokens, pureClaim, neFileName):
    noun = []
    verb = []
    adj = []

    #wordsWithPos = nltk.pos_tag(nltk.word_tokenize(pureClaim))
    #namedEntitiesSearch = nltk.ne_chunk(wordsWithPos, binary=True)
    #namedEntities = [ne for ne in namedEntitiesSearch if type(ne) is nltk.tree.Tree]
    #strEntities = []
    #for ne in namedEntities:
    #    phrase = ""
    #    for words in ne:
    #        phrase += words[0] + ' '
    #    strEntities.append( '\"' + phrase.strip() + '\"')
    namedEntities = readNamedEntitiesFromFile(neFileName)
    strEntities  = ["\"" + tx["text"] + "\"" for tx in namedEntities]

    #word structure [0] = word object = [lemma, fullword, POStag]; [1] tf-idf score
    for word in listOfTokens:
        wordExists = False
        for st in namedEntities:
            wLower = word[0][1].lower()
            neLower = st["text"].lower()

            if wLower == neLower:
                wordExists = True
                break

        if wordExists:
            continue
        if word[0][2][0] == 'V':
            verb.append(word[0][1])
        if word[0][2][0] == 'N':
            noun.append(word[0][1])
        if word[0][2][0] == 'J':
            adj.append(word[0][1])


    strEntities = strEntities[:3]
    noun = noun[:4]
    verb = verb[:3]

    if len(strEntities[:3]) + len(noun[:3]) + len(verb[:3]) <= 6:
        adj = adj[:3]
    else:
        adj = adj[:1]

    return [strEntities, noun, verb, adj]
