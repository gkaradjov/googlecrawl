import string

from nltk.corpus import stopwords as sw
from nltk.corpus import wordnet as wn
from nltk import wordpunct_tokenize
from nltk import WordNetLemmatizer
from nltk import sent_tokenize
from nltk import pos_tag
from nltk import TweetTokenizer
from sklearn.base import BaseEstimator, TransformerMixin



class NLTKPreprocessor(BaseEstimator, TransformerMixin):

    def __init__(self, tokenType, stopwords=None, punct=None,
                 lower=True, strip=True):
        self.lower      = lower
        self.strip      = strip
        self.stopwords  = stopwords or set(sw.words('english'))
        self.punct      = punct or set(string.punctuation)
        self.lemmatizer = WordNetLemmatizer()
        self.tokenType  = tokenType
        from nltk.tag.perceptron import PerceptronTagger
        self.tagger = PerceptronTagger()
        wn.ensure_loaded()

    def fit(self, X, y=None):
        return self

    def inverse_transform(self, X):
        return [" ".join(doc) for doc in X]

    def transform(self, X):
        return [
            list(self.tokenize(doc)) for doc in X
        ]

    def tokenize(self, document):
        if not self.tokenType or self.tokenType == 'regular':
            return self.tokenizeDocument(document)
        elif (self.tokenType == "regular-token-pos"):
            return self.tokenizeDocumentPOS(document)
        else:
            return self.tokenizeTweet(document)
        pass

    def preprocessString(self, document):
        if not self.tokenType or self.tokenType == 'regular':
            result = ' '.join(self.tokenizeDocument(document))
            return result
        elif (self.tokenType == "regular-token-pos"):
            return self.tokenizeDocumentPOS(document)
        else:
            return self.tokenizeTweet(document)
        pass

    def tokenizeTweet(self, document):
       tknzr = TweetTokenizer()

       for sent in sent_tokenize(document):
            # Break the sentence into part of speech tagged tokens
            tokens = self.tagger.tag(tknzr.tokenize(sent))
            for token, tag in tokens:
                # Apply preprocessing to the token
                token = token.lower() if self.lower else token
                token = token.strip() if self.strip else token
                token = token.strip('_') if self.strip else token
                token = token.strip('*') if self.strip else token

                # If stopword, ignore token and continue
                if token in self.stopwords:
                    continue

                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue

                # Lemmatize the token and yield
                lemma = self.lemmatize(token, tag)
                yield lemma

    def tokenizeDocument(self, document):
        result = self.tokenizeDocumentPOS(document)
        return [x[0] for x in result]

    def tokenizeDocumentRaw(self, document):
        # Break the document into sentences
        for sent in sent_tokenize(document):
            tokens = wordpunct_tokenize(sent)
            # Break the sentence into part of speech tagged tokens
            for token in tokens:
                rawToken = token
                # Apply preprocessing to the token
                token = token.lower() if self.lower else token
                token = token.strip() if self.strip else token
                token = token.strip('_') if self.strip else token
                token = token.strip('*') if self.strip else token

                # If stopword, ignore token and continue
                if token in self.stopwords:
                    continue

                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue

                yield rawToken

    def tokenizeDocumentPOS(self, document):
        # Break the document into sentences
        for sent in sent_tokenize(document):
            tokens = self.tagger.tag(wordpunct_tokenize(sent))
            # Break the sentence into part of speech tagged tokens
            for token, tag in tokens:
                rawToken = token
                # Apply preprocessing to the token
                token = token.lower() if self.lower else token
                token = token.strip() if self.strip else token
                token = token.strip('_') if self.strip else token
                token = token.strip('*') if self.strip else token

                # If stopword, ignore token and continue
                if token in self.stopwords:
                    continue

                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue

                # Lemmatize the token and yield
                lemma = self.lemmatize(token, tag)

                yield lemma, rawToken, tag

    def lemmatize(self, token, tag):
        tag = {
            'N': wn.NOUN,
            'V': wn.VERB,
            'R': wn.ADV,
            'J': wn.ADJ
        }.get(tag[0], wn.NOUN)

        return self.lemmatizer.lemmatize(token, tag)