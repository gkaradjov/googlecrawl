from Utils.data_access import *
import tldextract
import csv
import json

def getDomain(url):
    url = url.replace('https', 'http')
    url = url.replace('www.', '')
    if 'http://' not in url and 'https://' not in url:
        url = 'http://' + url

    extracted = tldextract.extract(url)
    base_url = "{}.{}".format(extracted.domain, extracted.suffix)

    if base_url[0] == '.':
        base_url = base_url[1:]

    return base_url





def getUniqueDomains():
    questions = get_claims()
    counter = 0
    setUnique = []
    for question in questions:
        data = {}

        my_file = Path('../Data/CrawledWebsites/Factchecking/Bing/' + str(question[0]) + '.json')
        if my_file.is_file():
            with open('../Data/CrawledWebsites/Factchecking/Bing/' + str(question[0]) + '.json', 'r+') as data_file:
                links = json.load(data_file)
            sites = [getDomain(s) for s in list(links['displayUrl'])]
            setUnique.extend(sites)

        my_file = Path('../Data/CrawledWebsites/Factchecking/Google/' + str(question[0]) + '.json')
        if my_file.is_file():
            with open('../Data/CrawledWebsites/Factchecking/Google/' + str(question[0]) + '.json', 'r+') as data_file:
                links = json.load(data_file)
            sites = [getDomain(s) for s in list(links['displayUrl'])]
            setUnique.extend(sites)

    result = [ (i,setUnique.count(i)) for i in set(setUnique) ]
    sorted_by_second = sorted(result, key=lambda tup: tup[1], reverse=True)
    with open('siteOcurancesFactChecking.tsv', 'w') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t', lineterminator='\n')
        for record in sorted_by_second:
            writer.writerow([str(record[0]), record[1]])

#getUniqueDomains()

def postProcess():
    result = []
    with open('../../Data/siteOcurancesAnnotated.tsv', 'r') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        next(tsvfile)
        currentBest = 0
        for record in reader:
            currentDataRecord = []
            currentDataRecord.append(record[0])
            numOfOccurances = int(record[1])
            if numOfOccurances > currentBest:
                currentBest = numOfOccurances
            currentDataRecord.append(numOfOccurances/currentBest)
            if record[2] == 'yes':
                currentDataRecord.append(1)
            else:
                currentDataRecord.append(0)

            if record[3] == 'reputed-source':
                currentDataRecord.append(1)
                currentDataRecord.append(0)
                currentDataRecord.append(0)
            elif record[3] == 'forum-type':
                currentDataRecord.append(0)
                currentDataRecord.append(1)
                currentDataRecord.append(0)
            else:
                currentDataRecord.append(0)
                currentDataRecord.append(0)
                currentDataRecord.append(1)
            result.append(currentDataRecord)

    with open('../../Data/siteOcurancesprocessed.tsv', 'w') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t', lineterminator='\n')
        for record in result:
            writer.writerow(record)



