from pathlib import Path
import csv, json


def readQLFile():
    claims = []
    my_file = Path('../Data/semeval2016-dev+test-with-annotations-clear-true-false-only.xml.tab_format')
    counter = 0
    if my_file.is_file():
        with open('../Data/semeval2016-dev+test-with-annotations-clear-true-false-only.xml.tab_format', 'r+') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            QID = ''
            Question = ''
            for rows in reader:
                if rows[1] == 'NA':
                    QID = rows[0]
                    Question = rows[2]
                else:
                    claims.append([QID,rows[0],Question, rows[2], rows[1]])
                counter+=1
    return claims

def get_claims():
    claims = []
    my_file = Path('../Data/event_claim_clean.csv')
    if my_file.is_file():
        with open('../Data/event_claim_clean.csv', 'r+') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for rows in reader:
                claims.append(rows)
    return claims

def get_tweets():
    claims = get_claims()
    result = []
    for claim in claims:
        result.append([claim[0],claim[1], get_tweets_for_claim(claim[0])])
    return result

def get_tweets_for_claim(claim_id):
    tweets = []
    my_file = Path('../Data/twitter_json/E' + claim_id + '.json')
    if my_file.is_file():
        with open('../Data/twitter_json/E' + claim_id + '.json', 'r+') as jsonfile:
            for line in jsonfile:
                reader = json.loads(line)
                tweets.append(reader['text'])
    return tweets


def writeQLFile(data, filename):

    with open(filename,'w', newline='') as csvFile:
        writer = csv.writer(csvFile, delimiter = '\t')
        writer.writerow(["QID", "CID", "Question", "Comment", "Label", "Query_QuestionComment", "Query_Question", "Query_comment"])
        for item in data:
            writer.writerow(item)


def writeSnopesFile(data, filename):
    with open(filename,'w', newline='') as csvFile:
        writer = csv.writer(csvFile, delimiter='\t')
        writer.writerow(["CID", "Claim", "Label", "Query"])
        for item in data:
            writer.writerow(item)