

def find_between(s, first, last):
    decodedInput = s.decode('utf-8')
    try:
        start = decodedInput.index(first) + len(first)
        end = decodedInput.index(last, start)
        return decodedInput[start:end]
    except ValueError:
        return ""

