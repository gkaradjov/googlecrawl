import json
from FactChecking.statistics import *
from FactChecking.similarities import get_most_simillar_sentences

def get_link_vectors(links, link_stats):
    result = []
    tempFound = False
    for link in links:
        tempFound = False
        for link_stat in link_stats:
            if link_stat[0] in getDomain(link):
                result.append([int(link_stat[2]),int(link_stat[3]),int(link_stat[4]),int(link_stat[5])])
                tempFound = True
                break
        if tempFound == False:
            result.append([0,0,0,1])
    return result

def get_similarity_file(entity):
     with open('../Data/Features/QL/Bing/SimilaritiesWithWebPagesNew/' + str(entity) + '.json', 'r+') as data_file:
        dataBing = json.load(data_file)

     htmlPath = '../Data/CrawledWebsites/QL/Bing/NewQuerying/' + str(entity) + '.json'
     my_file = Path(htmlPath)
     if my_file.is_file():
         with open(htmlPath, 'r+') as data_file:
            websites = json.load(data_file)
            processedHtml = websites["processedHtml"]
            dataBing["processedHtml"] = processedHtml
     return dataBing

def get_prediction(entity):
    with open('../Data/QL-Predictions/predicted-labels-dev+test-IRF_Bing_webpage_splitted-incl-map.tsv', 'r') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
           if row[0] == entity:
               return row[1]
    return "NULL"

def get_website_class(website):
    link_stats = []
    with open('../Data/siteOcurancesprocessed.tsv', 'r') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
           link_stats.append(row)
    vec = get_link_vectors([website], link_stats)[0]
    if vec[1] == 1:
        return "reputed"
    if vec[2] == 1:
        return "forum"
    else:
        return "other"


def filter_object(metric, new_data, qatar_related_vec, similarity_object):
    new_data[metric] = []
    if metric in similarity_object:
        for index, item in enumerate(similarity_object[metric]):
            if index in qatar_related_vec:
                new_data[metric].append(item)

def get_most_simillar_websites(similarity_object, metric):
    link_stats = []
    import copy
    new_data = copy.deepcopy(similarity_object)
    with open('../Data/siteOcurancesprocessed.tsv', 'r') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
           link_stats.append(row)

    link_vector = get_link_vectors(new_data["displayUrl"], link_stats)

    qatar_related_vec = []
    for index, item in enumerate(link_vector):
        #if item[0] == 1:
        qatar_related_vec.append(index)
    topSites = []
    if qatar_related_vec:
        filter_object(metric, new_data, qatar_related_vec, similarity_object)
        filter_object("displayUrl", new_data, qatar_related_vec, similarity_object)
        filter_object("snippets", new_data, qatar_related_vec, similarity_object)
        filter_object("processedHtml", new_data, qatar_related_vec, similarity_object)

        sites = new_data[metric]
        topSites = sorted(range(len(sites)), key=lambda i: sites[i])[-3:]

    return new_data, topSites



def get_website_stats(similarity_object, topWebsites, index, row):
    import sys
    if index < len(topWebsites):
        #Get URL
        top_index = topWebsites[index]
        url = similarity_object["displayUrl"][top_index]
        row.append(url)

        #Get snippet
        snippetText = similarity_object["snippets"][top_index].encode('utf-8', errors='ignore').decode('utf-8')
        print(snippetText)
        row.append(snippetText)

        #Credibility
        clas = get_website_class(url)
        row.append(clas)

        #Similarities
        row.append(similarity_object["webPageToquestionAnswerSimilarityWordVectorsMax"][top_index])

        #Best matched sentences
        row.append(get_most_simillar_sentences(similarity_object, similarity_object, top_index, 0))
    else:
        row.extend(['NULL', 'NULL', 'NULL', 'NULL', 'NULL'])
    pass

from Utils.data_access import *





def generate():
    questions = readQLFile()
    result = []
    for question in questions:
        row = []
        comment_id = question[1]
        row.append(comment_id)
        prediction = get_prediction(comment_id)
        row.append(prediction)
        actual = question[4]
        if actual == "True":
            row.append("1")
        else:
            row.append("0")


        similarity_object = get_similarity_file(comment_id)
        row.append(similarity_object["searchTerm"])
        new_object, top_website_indexes = get_most_simillar_websites(similarity_object, "webPageToquestionAnswerSimilarityWordVectorsMax")
        #print(top_website_indexes)
        for i in range(3):
            get_website_stats(new_object, top_website_indexes, i, row)
        result.append(row)
    with open('../Data/QL-Predictions/dev-test_all-map.tsv', 'w', encoding='utf-8') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t', lineterminator='\n')
        writer.writerow(["comment_id", "prediction", "actual", "search term",
                         "web_site_1_url", "web_site_1_snippet", "web_site_1_source", "web_site_1_similarity", "web_site_1_sentences",
                         "web_site_2_url", "web_site_2_snippet", "web_site_2_source", "web_site_2_similarity", "web_site_2_sentences",
                         "web_site_3_url", "web_site_3_snippet", "web_site_3_source", "web_site_3_similarity", "web_site_3_sentences",])

        for record in result:
            try:
                writer.writerow(record)
            except:
                print()




generate()