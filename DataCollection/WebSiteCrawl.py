import urllib.request

from bs4 import BeautifulSoup

from Utils.data_access import *

# -*- coding: utf-8 -*-
import sys
import json

def get_all_content(url):
    try:
        html = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(html)

        # kill all script and style elements
        for script in soup(["script", "style", "head"]):
            script.extract()    # rip it out
        # get text
        text = soup.get_text()

        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        return html, text.encode(sys.stdout.encoding, errors='replace')
    except:
        print("error - continue working")
        return "Error", "Error"

def get_html_content(html):
    try:
        import re
        soup = BeautifulSoup(html, "html.parser")
        # kill all script and style elements
        for script in soup(["script", "style"]):
            script.extract()    # rip it out
        # get text
        text = soup.get_text()

        text = text.\
            replace('\\n', '')\
            .replace('\\r', '')\
            .replace('\\t', '')\
            .replace('\\', '')\
            .replace('\\"', '')\
            .replace('\"', '')\
            .replace('b\\', '')\
            .replace('\\', '')


        text = repr(text)
        text = re.sub(r'\\[tn]', '', text)
        text = re.sub(r'\\x..', '', text)
        text = re.sub(' +',' ',text)

        return text.encode(sys.stdout.encoding, errors='ignore').decode("utf-8")
    except Exception as e:
        print(str(e))

def scrapWebsites():
    claims = get_claims()
    for claim in claims:
        data = {}
        data['event_id'] = claim[0]
        data['classification'] = claim[1]
        data['claim_heading'] = claim[2]
        links = []
        my_file = Path('../../Data/GoogleWebsitesLinks/' + str(claim[1]) + '.json')
        if my_file.is_file() and int(claim[0]) > 552:

                with open( '../../Data/GoogleWebsitesLinks/' + str(claim[0]) + '.json', 'r+') as data_file:
                    links = json.load(data_file)
                data['links'] = list(links[0][1])
                data['pureHtml'] = []
                for link in data['links']:
                    try:
                        html = 'not_parsed'
                        site = urllib.request.urlopen(link)
                        meta = site.info()
                        size = [item[1] for item in meta._headers if item[0] == 'Content-Length']
                        if not size or int(size[0]) < (10485760):
                            html = site.read()
                        else:
                            html = 'File too big'
                        data['pureHtml'].append(str(html))
                    except Exception as e:
                        print(str(e))

                resultString = json.dumps(data, sort_keys=True, indent=2)

                with open('../../Data/CrawledWebsites/' + str(claim[0]) + '.json', 'w') as outfile:
                    outfile.write(resultString)

def scrapWebsitesQL(question):
        data = {}
        data['comment_id'] = question[0]
        links = []
        my_file = Path('../Data/GoogleWebsiteLinks/QL/QLOnly/' + str(question[1]) + '.json')
        outputFilePath = Path('../Data/CrawledWebsites/QL/Google/QLOnly/' + str(question[1]) + '.json')

        #my_file = Path('../Data/GoogleWebsiteLinks/FactChecking/' + str(question[0]) + '.json')
        #outputFilePath = Path('../Data/CrawledWebsites/Factchecking/Google/' + str(question[0]) + '.json')
        if not outputFilePath.is_file():
            if my_file.is_file():
                    with open('../Data/GoogleWebsiteLinks/QL/QLOnly/' + str(question[1]) + '.json', 'r+') as data_file:
                    #with open('../Data/GoogleWebsiteLinks/FactChecking/' + str(question[0]) + '.json', 'r+') as data_file:
                        links = json.load(data_file)
                    data['links'] = list(links['links'])
                    data['snippets'] = list(links['snippets'])
                    data['displayUrl'] = list(links['displayUrl'])
                    data['query'] = links["query"]

                    #data['links'] = list(links[0][1])
                    #data['snippets'] = list(links[1][1])
                    #data['displayUrl'] = list(links[2][1])

                    data['pureHtml'] = []
                    data['processedHtml'] = []

                    if False:
                        for link in data['links']:
                            html = ''
                            pureText = ''
                            try:
                                site = urllib.request.urlopen(link)
                                meta = site.info()
                                size = [item[1] for item in meta._headers if item[0] == 'Content-Length']
                                if not size or int(size[0]) < (5485760):
                                    html = site.read()
                                    pureText = get_html_content(html)
                            except Exception as e:
                                print(str(e))
                            data['pureHtml'].append(str(html))
                            data['processedHtml'].append(str(pureText))

                    resultString = json.dumps(data, sort_keys=True, indent=2)

                    with open('../Data/CrawledWebsites/QL/Google/QLOnly/' + str(question[0]) + '.json', 'w') as outfile:
                        outfile.write(resultString)
#scrapWebsites()
def get_inner_html():
    claims = get_claims()
    for claim in claims:
        my_file = Path('../Data/CrawledWebsites/' + str(claim[0]) + '.json')
        if my_file.is_file():
            with open('../Data/CrawledWebsites/' + str(claim[0]) + '.json', 'r+' ) as jsonfile:
                try:
                    reader = jsonfile.read()
                    jsonF = json.loads(reader)
                    jsonF['processedHtml'] = []
                    for html in jsonF['pureHtml']:
                        inner = get_html_content(html)
                        jsonF['processedHtml'].append(inner)
                    output = json.dumps(jsonF, sort_keys=True, indent=2)
                    with open('../Data/CrawledWebsitesWithHtml/' + str(claim[0]) + '.json', 'w') as outfile:
                        outfile.write(output)
                    print()
                except Exception as e:
                    print(str(e))

questions = readQLFile()
#from Utils import MultiThreading as mt
#mt.map(scrapWebsitesQL,questions)

for question in questions:
    print(str(question[1]))
    scrapWebsitesQL(question)
